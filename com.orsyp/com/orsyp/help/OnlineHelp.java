package com.orsyp.help;

public class OnlineHelp {
	public static void displayHelp(int RC, String Message, String Release) {
		
		System.out.println(Message+"\n");
		System.out.println("-----------------------------------------------------------------------------------");
		System.out.println("   **               ORSYP Alert Rule Management CLSA "+Release);	
		System.out.println("-----------------------------------------------------------------------------------\n");
		System.out.println("");
		System.out.println("++ Optional Parameters:\n");
		System.out.println("    --login=:     UVMS connection login     [can be specified in properties file]");
		System.out.println("    --pwd=:       UVMS connection password  [can be specified in properties file]");
		System.out.println("    --uvms=:      UVMS hostname             [can be specified in properties file]");
		System.out.println("    --port=:      UVMS Connection port      [can be specified in properties file]");
		System.out.println("    --filter=:    Alert Text Filter         [can be specified in properties file]");
		System.out.println("");
		System.out.println("++ Mandatory Parameters:\n");
		System.out.println("    --enable=: [Y/N]      Enable Alerts / Disable Alerts that match filter");
		System.out.println("");
		System.out.println("++ Display, Help & Version Info:\n");
		System.out.println("    --help:    Display online help");
		System.out.println("    --version: Version Information & UVMS compatibility");
		System.out.println("");
		System.out.println("++ Examples:\n");
		System.out.println("    AlertsMgt --enable=Y");
		System.out.println("    AlertsMgt --uvms=bsaorshk02 --pwd=universe --login=admin --enable=Y");
		System.out.println("    AlertsMgt --uvms=bsaorshk02 --pwd=universe --login=admin --filter=SCHED --enable=Y");
		System.out.println("");
		System.out.println("  IMPORTANT NOTE:");
		System.out.println("  => All Configuration parameters can be modified from properties file in same folder as Jar file");
		System.out.println("");
		System.exit(RC);
	}

	public static void displayVersion(int RC, String release, String[] APIversions) {
		System.out.println("Version "+release);
		System.out.println("++ This Tool was compiled and tested with the following UVMS:");
		for (int j=0;j < APIversions.length;j++){
			System.out.println(APIversions[j]);
		}
		System.out.println("  => Other versions of UVMS might not work as expected or might not work at all");
		System.exit(RC);
	}
}
