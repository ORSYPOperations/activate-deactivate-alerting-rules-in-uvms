package com.orsyp;


import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;
import com.orsyp.std.central.AlertConfigurationListStdImpl;
import com.orsyp.std.central.AlertConfigurationStdImpl;
import com.orsyp.std.central.network.NetworkNodeListStdImpl;
import com.orsyp.api.central.AlertConfiguration;
import com.orsyp.api.central.AlertConfigurationItem;
import com.orsyp.api.central.AlertConfigurationList;
import com.orsyp.api.central.UniCentral;
import com.orsyp.api.central.UniCentral.LicenseEntryStatus;
import com.orsyp.api.central.networknode.NetworkNodeList;
import com.orsyp.help.OnlineHelp;
import edu.mscd.cs.jclo.JCLO;

/**
 * INTERNAL RELEASE NOTES:
 * 
 * 1.0.7: consistency check based on the number of entries per line and the detection of duplicates
 * 1.0.8: handling of duplicate entries in csv file
 * 1.0.9: fixing an issue where the node list returned by the UVMS is not correct
 **/

class AllArgs {
	  private boolean help;
	    private String pwd;
	    private String login;
	    private int port;
	    private String uvms;
	    private boolean version;
	    private boolean verbose;
	    private String filter;
	    private String enable;
}

public class GoAgt {

	//static variables
	
	static boolean Verbose;
	static boolean AlertEnabled;
	static String Customer = "CLSA";
	static String Release="0.0.1";
	static String[] APIversions = {"300"};
	static String PROPFILE="\\SchedAlerts.properties";
	
	public static void main(String[] argv) throws IOException, InterruptedException, InstantiationException, IllegalAccessException, UniverseException {
			
		String password="";
		String login="";
		String UVMSHost="";

		int port=0;
		boolean displayHelp=false;
		boolean displayVer=false;
		boolean verbose=false;
		String filter="";

			System.out.println("=======================================================");
			System.out.println("  ** ORSYP UVMS Alerts Management v  " + Release +"  ");
			System.out.println("  *          ORSYP Professional Services.            ");
			System.out.println("  * Copyright (c) 2012 ORSYP.  All rights reserved.  ");
			System.out.println("  **             Implemented for "+Customer+"              ");
			System.out.println("=======================================================");
			System.out.println("");
      
			JCLO jclo = new JCLO (new AllArgs());
	        jclo.parse (argv);
	        
	        

	        displayHelp=jclo.getBoolean ("help");
	        displayVer=jclo.getBoolean ("version");
	        password=jclo.getString ("pwd");
	        login=jclo.getString ("login");
	        UVMSHost=jclo.getString ("uvms");
	        port=jclo.getInt ("port");
	        verbose=jclo.getBoolean ("verbose");
	        filter=jclo.getString ("filter");
	        displayVer=jclo.getBoolean ("version");
	        String AlertEnabledS = jclo.getString ("enable");
	     
	            
	        if(verbose){Verbose=true;}

	        if(displayHelp){OnlineHelp.displayHelp(0,"Display help",Release);}
	        if(displayVer){OnlineHelp.displayVersion(0,Release,APIversions);}
		    
	       // Properties pGlobalVariables = PropertyLoader.loadProperties("UJAgtMgt");
	        Properties pGlobalVariables = Utils.loadFile(PROPFILE);

	        
	        if (password==null || password.equals("")){password=pGlobalVariables.getProperty("UVMS_PWD");}    
		    if (password==null || password.equals("")){OnlineHelp.displayHelp(-1,"Error: No Password Passed!",Release);}

		   
		    
			System.out.println("==> Loading Program Options ...");
            
			//if (MinNumberOfNodes==0){}
            if (login == null || login.equals("")){login=pGlobalVariables.getProperty("UVMS_USER");}
            if (UVMSHost == null || UVMSHost.equals("")){UVMSHost=pGlobalVariables.getProperty("UVMS_HOST");}
            if (port==0){port=Integer.parseInt(pGlobalVariables.getProperty("UVMS_PORT"));}
            
            if (filter == null || filter.equals("")){filter=pGlobalVariables.getProperty("UVMS_ALERT_LABEL_STARTS_WITH");}
            if (AlertEnabledS == null || AlertEnabledS.equals("")){AlertEnabledS="Y";}
            AlertEnabled=convertOption(AlertEnabledS);
            AlertEnabled=false;
            if(UVMSHost==null||password==null||login==null||UVMSHost==""||port==0||password==""||login==""){
            	System.out.println("-- Fatal Error in Parameters Passed.");
            	OnlineHelp.displayHelp(1,"Error in Parameters",Release);
            }
			
			
				UVMSInterface uvInterface = new UVMSInterface(UVMSHost,port,login,password,"NONE","UNIV56",Area.Exploitation);
				
				UniCentral central = uvInterface.ConnectEnvironment();	
				
				//System.out.println("AFTER CONNECT ENV METHOD");
				//System.exit(0);
				// lic check mechanism
		        boolean checkLicActive=false;
		   			if(checkLicActive){
		               // Check for Licence Validity Here !!
		   			
		   			String LicStr=pGlobalVariables.getProperty("MODULE_LIC");
		   			boolean RepLicOK=false;
		   			String LicStat="";
		   			//it's Ugly, but we are checking for a Publisher Licence here.
		   			LicStr="HOST=* NODE=* PRODUCT=UNIVIEWER VERSION=3.0 PUBLISHER "+LicStr;
	   				LicenseEntryStatus myLicStat= central.checkLicenseEntry(LicStr);  
		   				  if(myLicStat.toString().equals("VALID")){RepLicOK=true;}
		   				  LicStat=myLicStat.toString();	  
		   				
		   					  
		   			if(!RepLicOK){
		   				System.out.println("  --- ERROR - Licence for this Module on UVMS has Status: "+LicStat);
		   				System.exit(99);
		   			}
		   			System.out.println("  +++ Licence Valid until: "+LicStr.substring(60, 68)+"\n");
		   			
		   			}
					// end of lic check mechanism
				
					AlertConfigurationList alertList = new AlertConfigurationList(uvInterface.getContext());
					alertList.setImpl(new AlertConfigurationListStdImpl());
					try {
						alertList.extract();
					} catch (UniverseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//boolean AlertEnabled=true;
					
					int AlertCount=alertList.getCount();
					int AlertCountToProcess=0;
					for (int i=0; i<alertList.getCount();i++){

						if(alertList.get(i).getLabel().startsWith(filter)){AlertCountToProcess++;}
					}
					
					System.out.println("==> processing alerts ...");
					System.out.println("  +++ Number of Alerts Found: "+AlertCount);
					System.out.println("  +++ Number of Alerts Found Matching Criteria (To Process): "+AlertCountToProcess +"\n");
					
					for (int i=0; i<alertList.getCount();i++){
						if(alertList.get(i).getLabel().startsWith(filter)){
						System.out.println("  ==> Processing Alert: "+alertList.get(i).getLabel());
						String Status="";
						
						
						AlertConfigurationItem myIt = alertList.get(i);
						AlertConfiguration myAlert = myIt.getAlert() ;
						
						myAlert.setImpl(new AlertConfigurationStdImpl());
						myAlert.setContext(uvInterface.getContext());

						if(myAlert.isEnable()){Status=Status+"      Alert was in status Enabled; ";}
						if(!myAlert.isEnable()){Status=Status+"      Alert was in status Disabled; ";}

						myAlert.setEnable(AlertEnabled);
						myAlert.update();
						if(AlertEnabled){Status=Status+"Alert is now Enabled";}
						else{Status=Status+"Alert is now Disabled";}
						
						System.out.println(Status+"\n");
						}
						
					}
					System.out.println("==> End of Alert Processing");
	}
	public static boolean convertOption(String s){
		if (s.equalsIgnoreCase("O") || s.equalsIgnoreCase("Y")){
			return true;
		}
		if (s.equalsIgnoreCase("N")){
			return false;
		}
		return true;
	}

}