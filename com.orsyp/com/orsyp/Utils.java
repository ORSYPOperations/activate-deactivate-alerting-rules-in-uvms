package com.orsyp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;
import java.util.Random;

public class Utils {

	
	public Utils(){
		
		
	}
	
	public static String getCurrentTimeStamp(){
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		Calendar cal = Calendar.getInstance();
		return dateFormat.format(cal.getTime());
	}
	
	public static void printHeaderOA(){
		System.out.println("openagent\n");
	}
	
	public static void printDomainData(String metricName,String domainName,String date, String value){
		System.out.println(metricName+"#"+domainName+"#"+date+"#"+value);
	}
	
	
	public static boolean getRandomValue(int percentage){
		
		if(percentage> 100){percentage=100;}
		
		Random gen = new Random();
		int Num = gen.nextInt(101);
		//System.out.println("DEBUG:"+Num+":"+percentage);
		if(Num<percentage){return true;}
		
		return false;
	}

	public static long getRandNumInRange(int sLEEP_TIME_HEADER_MIN,
			int sLEEP_TIME_HEADER_MAX) {

		Random gen = new Random();
		int Num = gen.nextInt(sLEEP_TIME_HEADER_MAX+1);
		while(Num<sLEEP_TIME_HEADER_MIN){
			Num = gen.nextInt(sLEEP_TIME_HEADER_MAX+1);
		}
		return Num;
	}
	
	public static Properties loadFile(String s) {
		String path = null;
		String FilePath=null;
		Properties prop=new Properties ();
		try {
			path = new File(".").getCanonicalPath();
			FilePath=path+s;
		} catch (IOException e) {
			e.printStackTrace();
		}
		File myFile = new File(FilePath);
		InputStream in = null;
		try {
			in = new FileInputStream(myFile);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		
        try {
			prop.load (in);
		} catch (IOException e) {
			e.printStackTrace();
		} 
		return prop;
	}
}
